TARGET = passgen
VERSION = "2.1"

CC = cc
CFLAGS = -Wall -Wextra -Wpedantic -Werror -O3 -std=c99 -D_DEFAULT_SOURCE -DVERSION=\"$(VERSION)\"

MONTH = $(shell date +%B)
YEAR = $(shell date +%Y)

SRCS = passgen.c
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man
MANPAGE = $(TARGET).1

DEPS = $(SRCS:.c=.d)
-include $(DEPS)

all: $(TARGET)

$(TARGET): $(SRCS)
	$(CC) $(CFLAGS) -MMD -MP -o $@ $<

install: $(TARGET)
	@sed -i "s/\.TH PASSGEN 1 \".*\" \"passgen .*\"/.TH PASSGEN 1 \"$(MONTH) $(YEAR)\" \"passgen $(VERSION)\"/" $(MANPAGE)
	cp -f $(TARGET) $(PREFIX)/bin
	chmod 755 $(PREFIX)/bin/$(TARGET)
	cp -f $(MANPAGE) $(MANPREFIX)/man1/$(TARGET).1
	chmod 644 $(MANPREFIX)/man1/$(TARGET).1

uninstall:
	rm -f $(PREFIX)/bin/$(TARGET) $(MANPREFIX)/man1/$(TARGET).1

clean:
	rm -f $(TARGET) $(DEPS)

test: $(TARGET)
	@echo "Running tests..."
	@./$(TARGET) -L 16 | grep -qE '^.{16}$$' || (echo "Length test failed"; exit 1)
	@./$(TARGET) -l | grep -qE '^[^a-z]+$$' || (echo "Lowercase exclusion test failed"; exit 1)
	@./$(TARGET) -u | grep -qE '^[^A-Z]+$$' || (echo "Uppercase exclusion test failed"; exit 1)
	@./$(TARGET) -n | grep -qE '^[^0-9]+$$' || (echo "Number exclusion test failed"; exit 1)
	@./$(TARGET) -s | grep -qE '^[a-zA-Z0-9]+$$' || (echo "Symbol exclusion test failed"; exit 1)
	@echo "All tests passed"

.PHONY: all install uninstall clean test
