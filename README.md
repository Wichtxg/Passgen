# Passgen

**[Passgen](https://gitlab.com/wichtxg/passgen)** is a command-line tool for generating random passwords based on user-defined criteria.

# Compatibility
- Linux systems only
- Requires Linux kernel version 3.17 or newer

## Usage

### Generate a password

To generate a password, use the following command structure:

```sh
passgen [-L length] [-l] [-u] [-n] [-s]
```

### Running The tests
```sh
make test
```

### Options:

- `-L length`: Set password length (default: 16)
- `-l`: Exclude lowercase letters (a-z) from the generated password
- `-u`: Exclude uppercase letters (A-Z) from the generated password
- `-n`: Exclude numeric digits (0-9) from the generated password
- `-s`: Exclude special characters (e.g., !@#$%^&*()-_=+[]{}|;:,.<>?) from the generated password
- `-v`: Display version information
- `-N`: Do not print a newline character after the password
- `-c count`: Generate multiple passwords (default: 1)

By default, if no options are specified, the password will include all character types.

### Examples:
Generate a password of length 20 with all character types: 

```sh
passgen -L 20
```

Generate a password of length 12 with only uppercase letters and digits: 
```sh
passgen -L 12 -ls
```

Generate a password of length 16 with lowercase letters, digits, and special characters: 
```sh
passgen -L 16 -u
```

Generate 5 passwords:
```sh
passgen -c 5
```

Generate a password without newline (useful for scripts):
```sh
passgen -N
```

## Installation

You need to have [make](https://www.gnu.org/software/make/) installed and a C compiler (gcc is recommended). 
```sh
git clone https://gitlab.com/wichtxg/passgen --depth=1
cd passgen
sudo make install
```

For more detailed documentation, read the manual or see the help page by running: 
```sh
passgen -h
```

## Uninstallation
```sh
cd path/to/passgen
sudo make uninstall
cd ..
rm -rf passgen/
```

## Security Considerations
- Uses `getrandom()` for cryptographically secure random number generation
- Implements modulo bias protection for uniform distribution
- Validates all inputs to prevent buffer overflows
- No dynamic memory allocation to prevent memory leaks
