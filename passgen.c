#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/random.h>

#define MAX_LENGTH 128
#define MIN_LENGTH 1
#define DEFAULT_LENGTH 16

static const char help[] = 
    "Usage: passgen [-L len] [-l] [-u] [-n] [-s] [-v] [-N] [-c count]\n"
    "  -L len   Set password length (1-128, default: 16)\n"
    "  -l       Exclude lowercase letters\n"
    "  -u       Exclude uppercase letters\n"
    "  -n       Exclude numbers\n"
    "  -s       Exclude symbols\n"
    "  -N       No newline\n"
    "  -c count Generate multiple passwords\n"
    "  -v       Show version\n"
    "  -h       Show this help\n";

static const char* const charset[] = {
    "", "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "0123456789", "abcdefghijklmnopqrstuvwxyz0123456789", "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", "!@#$%^&*()-_=+[]{}|;:,.<>?",
    "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-_=+[]{}|;:,.<>?", "ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()-_=+[]{}|;:,.<>?",
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()-_=+[]{}|;:,.<>?",
    "0123456789!@#$%^&*()-_=+[]{}|;:,.<>?", "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+[]{}|;:,.<>?",
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:,.<>?",
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:,.<>?"
};

static inline void die(const char *s, int x) { fputs(s, stderr); exit(x); }

int main(int argc, char *argv[]) {
    size_t len = DEFAULT_LENGTH, count = 1;
    int flag = 15, nl = 1, opt;
    unsigned char c;
    const char *cs;
    size_t cslen;

    while ((opt = getopt(argc, argv, "L:lunsvNc:h")) != -1)
        switch (opt) {
            case 'L': len = atoi(optarg); break;
            case 'l': flag &= ~1; break;
            case 'u': flag &= ~2; break;
            case 'n': flag &= ~4; break;
            case 's': flag &= ~8; break;
            case 'N': nl = 0; break;
            case 'c': count = atoi(optarg); break;
            case 'v': puts("passgen version: " VERSION); return 0;
            default: die(help, opt=='h'?0:1);
        }

    if (len < MIN_LENGTH || len > MAX_LENGTH) die("Error: Invalid length.\n", 1);
    if (count < 1) die("Error: Invalid count.\n", 1);
    if (!flag) die("Error: No charset selected.\n", 1);

    cs = charsets[flag];
    if (!(cslen = strlen(cs))) die("Error: Empty charset.\n", 1);

    while (count--) {
        for (size_t i = 0; i < len; i++) {
            do {
                if (getrandom(&c, 1, 0) == -1) die("Error: RNG failed.\n", 1);
            } while (c >= (256 - (256 % cslen)));
            if (putchar(cs[c % cslen]) == EOF) die("Error: Write failed.\n", 1);
        }
        if (nl && putchar('\n') == EOF) die("Error: Write failed.\n", 1);
    }

    return 0;
}
